EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:teensy_3.1
LIBS:charger
LIBS:switch_dpdt
LIBS:oled_i2c
LIBS:bluetooth
LIBS:fs177
LIBS:w_rtx
LIBS:tmp36
LIBS:bicycle-companion-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SWITCH_DPDT SW5
U 1 1 57B9DE29
P 5600 3550
F 0 "SW5" H 5600 4000 70  0000 C CNN
F 1 "MAIN_SWITCH" H 5600 3100 70  0000 C CNN
F 2 "dpdt_switch:DPDT_ESWITCH_EG2207" H 5600 3550 60  0001 C CNN
F 3 "" H 5600 3550 60  0001 C CNN
	1    5600 3550
	1    0    0    -1  
$EndComp
Text Label 3250 2000 0    60   ~ 0
Vin
Wire Wire Line
	3250 2000 3100 2000
Text Label 5000 3350 2    60   ~ 0
Vin
Wire Wire Line
	5000 3350 5100 3350
Text Label 5000 3750 2    60   ~ 0
Vusb
Text Label 6200 3250 0    60   ~ 0
L+
Text Label 6200 3450 0    60   ~ 0
Vusb
Text Label 6200 3850 0    60   ~ 0
C+
Wire Wire Line
	6100 3250 6200 3250
Wire Wire Line
	6200 3450 6100 3450
Wire Wire Line
	6100 3850 6200 3850
Wire Wire Line
	5000 3750 5100 3750
Text Label 5050 1400 2    60   ~ 0
L+
Text Label 4250 1400 2    60   ~ 0
C+
NoConn ~ 6100 3650
Wire Wire Line
	950  2000 1100 2000
Text Label 2350 1550 0    60   ~ 0
Vusb
Text Notes 3500 5100 0    59   ~ 0
En posicion normal, Vin está alimentado por la bateria (L+) y\nVusb está desconectado (la PC no alimenta ni carga).\nEn posición alternativa, Vin conecta con Vusb (USB alimenta Teensy)\ny a la vez Vusb carga a través de C+. En este caso (posicion de carga)\nno se usa la bateria mientras se carga.
Text Label 4150 3400 0    60   ~ 0
Vin
Text Label 3250 2300 0    60   ~ 0
VCC
Text Label 6950 2100 2    60   ~ 0
VCC
Text Label 6950 2200 2    60   ~ 0
SCL
Text Label 6950 2300 2    60   ~ 0
SDA
Text Label 3250 3050 0    60   ~ 0
SCL
Text Label 3250 3200 0    60   ~ 0
SDA
Wire Wire Line
	3100 3050 3250 3050
Wire Wire Line
	3100 3200 3250 3200
Text Label 5150 2700 2    60   ~ 0
SDA
Text Label 5650 2700 0    60   ~ 0
SCL
$Comp
L R R3
U 1 1 57B9F30C
P 5300 2450
F 0 "R3" V 5380 2450 50  0000 C CNN
F 1 "4k" V 5300 2450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 5230 2450 50  0001 C CNN
F 3 "" H 5300 2450 50  0000 C CNN
	1    5300 2450
	-1   0    0    1   
$EndComp
Text Label 5400 2200 0    60   ~ 0
VCC
$Comp
L R R4
U 1 1 57B9F398
P 5500 2450
F 0 "R4" V 5580 2450 50  0000 C CNN
F 1 "4k" V 5500 2450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 5430 2450 50  0001 C CNN
F 3 "" H 5500 2450 50  0000 C CNN
	1    5500 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 2300 5300 2250
Wire Wire Line
	5300 2250 5500 2250
Wire Wire Line
	5400 2250 5400 2200
Wire Wire Line
	5500 2250 5500 2300
Wire Wire Line
	5500 2600 5500 2700
Wire Wire Line
	5500 2700 5650 2700
Wire Wire Line
	5300 2600 5300 2700
Wire Wire Line
	5300 2700 5150 2700
Text Notes 1100 950  0    60   ~ 0
IMPORTANTE: Vusb y Vin estan desconectados\n(trazo cortado en PCB)
Text Label 7400 4850 2    60   ~ 0
VCC
Text Label 7400 4950 2    60   ~ 0
HALL
Text Label 1100 2450 2    60   ~ 0
HALL
$Comp
L R R1
U 1 1 57C10854
P 4150 3600
F 0 "R1" V 4050 3600 50  0000 C CNN
F 1 "10k" V 4150 3600 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4080 3600 50  0001 C CNN
F 3 "" H 4150 3600 50  0000 C CNN
	1    4150 3600
	-1   0    0    1   
$EndComp
$Comp
L R R2
U 1 1 57C108A3
P 4150 3950
F 0 "R2" V 4230 3950 50  0000 C CNN
F 1 "10k" V 4150 3950 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4080 3950 50  0001 C CNN
F 3 "" H 4150 3950 50  0000 C CNN
	1    4150 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3400 4150 3400
Text Label 1050 2600 2    60   ~ 0
TEENSY_TX
Text Label 1050 2750 2    60   ~ 0
TEENSY_RX
Wire Wire Line
	1050 2600 1100 2600
Wire Wire Line
	1050 2750 1100 2750
Text Label 7300 4150 2    60   ~ 0
TEENSY_RX
Text Label 7300 4250 2    60   ~ 0
TEENSY_TX
Wire Wire Line
	7300 4150 7400 4150
Wire Wire Line
	7300 4250 7400 4250
Text Label 8250 4000 2    60   ~ 0
BT_RESET
Wire Wire Line
	8250 3900 8350 3900
Text Label 8250 3900 2    60   ~ 0
VCC
Wire Wire Line
	8250 4000 8350 4000
Text Label 7300 2900 2    60   ~ 0
BT_STATUS
Wire Wire Line
	7400 2800 7300 2800
NoConn ~ 2200 4550
NoConn ~ 2350 4550
NoConn ~ 1100 3500
Wire Wire Line
	3100 2300 3250 2300
Text Label 2750 6700 0    60   ~ 0
GND
Text Label 3650 7150 0    60   ~ 0
BUT1
Text Label 1750 6850 2    60   ~ 0
BUT2
Text Label 1750 6550 2    60   ~ 0
BUT3
Text Label 3650 6200 0    60   ~ 0
BUT4
Text Label 950  2000 2    60   ~ 0
GND
NoConn ~ 2050 4550
NoConn ~ 3100 2150
Text Label 3250 2600 0    60   ~ 0
BUT1
Text Label 1000 3800 2    60   ~ 0
BUT2
Text Label 950  2150 2    60   ~ 0
BUT3
Text Label 3250 3950 0    60   ~ 0
BUT4
Wire Wire Line
	1000 3800 1100 3800
Text Label 7400 4750 2    60   ~ 0
GND
Text Label 4300 4200 0    60   ~ 0
GND
Wire Wire Line
	4150 4100 4150 4200
Wire Wire Line
	4150 4200 4300 4200
Wire Wire Line
	4150 3450 4150 3400
NoConn ~ 1100 3350
Text Label 3250 3350 0    60   ~ 0
C+
Wire Wire Line
	1000 3200 1100 3200
Text Label 1000 3050 2    60   ~ 0
BT_STATUS
Text Label 3250 2450 0    60   ~ 0
BT_RESET
Wire Wire Line
	1000 3050 1100 3050
NoConn ~ 3100 2900
NoConn ~ 3100 2750
NoConn ~ 1100 3950
NoConn ~ 1100 2900
NoConn ~ 1100 3650
NoConn ~ 1100 2300
Wire Wire Line
	1750 4550 1750 5000
Text Label 1750 5100 0    60   ~ 0
GND
$Comp
L CONN_01X02 P1
U 1 1 57D58219
P 1500 5050
F 0 "P1" H 1500 5200 50  0000 C CNN
F 1 "RTC_BATTERY" V 1600 5050 50  0000 C CNN
F 2 "wire_pads:SolderWirePad_2x_0-8mmDrill" H 1500 5050 50  0001 C CNN
F 3 "" H 1500 5050 50  0000 C CNN
	1    1500 5050
	-1   0    0    1   
$EndComp
Wire Wire Line
	1700 5100 1750 5100
$Comp
L Teensy_3.1 U1
U 1 1 57D5952D
P 2100 3300
F 0 "U1" H 2100 3750 60  0000 C CNN
F 1 "Teensy_3.1" H 2100 3900 60  0000 C CNN
F 2 "Teensy:Teensy3.x_LC" H 2200 3300 60  0001 C CNN
F 3 "" H 2200 3300 60  0000 C CNN
	1    2100 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 6200 3500 6200
Wire Wire Line
	1750 6550 1850 6550
Wire Wire Line
	1750 6850 1850 6850
Wire Wire Line
	3650 7150 3500 7150
$Comp
L CONN_01X03 P2
U 1 1 57D5D363
P 7700 4850
F 0 "P2" H 7700 5050 50  0000 C CNN
F 1 "HALL" V 7800 4850 50  0000 C CNN
F 2 "wire_pads:SolderWirePad_3x_0-8mmDrill" H 7700 4850 50  0001 C CNN
F 3 "" H 7700 4850 50  0000 C CNN
	1    7700 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 4750 7500 4750
Wire Wire Line
	7400 4850 7500 4850
Wire Wire Line
	7500 4950 7400 4950
Text Notes 3800 900  0    60   ~ 0
los cables de la bateria van soldados al cargador
Text Label 5050 1500 2    60   ~ 0
GND
Text Label 6950 2000 2    60   ~ 0
GND
Text Label 4250 1500 2    60   ~ 0
GND
Wire Wire Line
	1750 5000 1700 5000
$Comp
L SW_PUSH SW1
U 1 1 57D62343
P 3200 6200
F 0 "SW1" H 3350 6310 50  0000 C CNN
F 1 "SW_PUSH" H 3200 6120 50  0000 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_Tactile_SPST_Angled" H 3200 6200 50  0001 C CNN
F 3 "" H 3200 6200 50  0000 C CNN
	1    3200 6200
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH SW2
U 1 1 57D623B6
P 2150 6550
F 0 "SW2" H 2300 6660 50  0000 C CNN
F 1 "SW_PUSH" H 2150 6470 50  0000 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_Tactile_SPST_Angled" H 2150 6550 50  0001 C CNN
F 3 "" H 2150 6550 50  0000 C CNN
	1    2150 6550
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH SW3
U 1 1 57D623F5
P 2150 6850
F 0 "SW3" H 2300 6960 50  0000 C CNN
F 1 "SW_PUSH" H 2150 6770 50  0000 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_Tactile_SPST_Angled" H 2150 6850 50  0001 C CNN
F 3 "" H 2150 6850 50  0000 C CNN
	1    2150 6850
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH SW4
U 1 1 57D6244A
P 3200 7150
F 0 "SW4" H 3350 7260 50  0000 C CNN
F 1 "SW_PUSH" H 3200 7070 50  0000 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_Tactile_SPST_Angled" H 3200 7150 50  0001 C CNN
F 3 "" H 3200 7150 50  0000 C CNN
	1    3200 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 6200 2600 6200
Wire Wire Line
	2600 6200 2600 7150
Wire Wire Line
	2600 6550 2450 6550
Wire Wire Line
	2600 6850 2450 6850
Wire Wire Line
	2600 7150 2900 7150
Connection ~ 2600 6550
Connection ~ 2600 6850
Wire Wire Line
	2600 6700 2750 6700
Connection ~ 2600 6700
$Comp
L CONN_01X02 P6
U 1 1 57D5D854
P 7600 2850
F 0 "P6" H 7600 3000 50  0000 C CNN
F 1 "CONN_01X02" V 7700 2850 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 7600 2850 50  0001 C CNN
F 3 "" H 7600 2850 50  0000 C CNN
	1    7600 2850
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P8
U 1 1 57D5D88D
P 7600 4200
F 0 "P8" H 7600 4350 50  0000 C CNN
F 1 "CONN_01X02" V 7700 4200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 7600 4200 50  0001 C CNN
F 3 "" H 7600 4200 50  0000 C CNN
	1    7600 4200
	1    0    0    -1  
$EndComp
Text Label 7300 2800 2    60   ~ 0
BT_CTRL
Wire Wire Line
	7300 2900 7400 2900
$Comp
L TMP36 U2
U 1 1 57D60872
P 4500 2350
F 0 "U2" H 4500 2500 60  0000 C CNN
F 1 "TMP36" H 4500 2600 60  0000 C CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 4500 2450 60  0001 C CNN
F 3 "" H 4500 2450 60  0001 C CNN
	1    4500 2350
	1    0    0    -1  
$EndComp
Text Label 3250 3500 0    60   ~ 0
TEMP
Wire Wire Line
	3100 3500 3250 3500
Text Label 4500 2750 3    60   ~ 0
TEMP
Text Label 4400 2750 3    60   ~ 0
VCC
Text Label 4600 2750 3    60   ~ 0
GND
Wire Wire Line
	4400 2750 4400 2650
Wire Wire Line
	4500 2750 4500 2650
Wire Wire Line
	4600 2750 4600 2650
$Comp
L CONN_01X04 P5
U 1 1 57D625FE
P 7200 2150
F 0 "P5" H 7200 2400 50  0000 C CNN
F 1 "CONN_DISP" V 7300 2150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 7200 2150 50  0001 C CNN
F 3 "" H 7200 2150 50  0000 C CNN
	1    7200 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 2000 7000 2000
Wire Wire Line
	6950 2100 7000 2100
Wire Wire Line
	6950 2200 7000 2200
Wire Wire Line
	6950 2300 7000 2300
$Comp
L CONN_01X02 P3
U 1 1 57D63CD2
P 4550 1450
F 0 "P3" H 4550 1600 50  0000 C CNN
F 1 "CONN_CHARGE" V 4650 1450 50  0000 C CNN
F 2 "wire_pads:SolderWirePad_2x_0-8mmDrill" H 4550 1450 50  0001 C CNN
F 3 "" H 4550 1450 50  0000 C CNN
	1    4550 1450
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P4
U 1 1 57D63D0D
P 5300 1450
F 0 "P4" H 5300 1600 50  0000 C CNN
F 1 "CONN_DISCH" V 5400 1450 50  0000 C CNN
F 2 "wire_pads:SolderWirePad_2x_0-8mmDrill" H 5300 1450 50  0001 C CNN
F 3 "" H 5300 1450 50  0000 C CNN
	1    5300 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 1400 4350 1400
Wire Wire Line
	4250 1500 4350 1500
Wire Wire Line
	5050 1400 5100 1400
Wire Wire Line
	5050 1500 5100 1500
$Comp
L CONN_01X01 P7
U 1 1 57D5F10A
P 8400 3300
F 0 "P7" H 8400 3400 50  0000 C CNN
F 1 "CONN_01X01" V 8500 3300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 8400 3300 50  0001 C CNN
F 3 "" H 8400 3300 50  0000 C CNN
	1    8400 3300
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P9
U 1 1 57D5F179
P 8550 3950
F 0 "P9" H 8550 4100 50  0000 C CNN
F 1 "CONN_01X02" V 8650 3950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 8550 3950 50  0001 C CNN
F 3 "" H 8550 3950 50  0000 C CNN
	1    8550 3950
	1    0    0    -1  
$EndComp
Text Label 8200 3300 2    60   ~ 0
GND
NoConn ~ 3100 3650
Text Label 1000 3200 2    60   ~ 0
BT_CTRL
Wire Wire Line
	3250 2450 3100 2450
Wire Wire Line
	950  2150 1100 2150
Wire Wire Line
	3100 2600 3250 2600
Wire Wire Line
	3100 3950 3250 3950
NoConn ~ 3100 3800
Text Label 3950 3750 2    60   ~ 0
Vin_MEAS
Wire Wire Line
	4150 3750 3950 3750
Wire Wire Line
	4150 3750 4150 3800
Text Label 2050 1550 1    60   ~ 0
Vin_MEAS
NoConn ~ 2200 1550
NoConn ~ 1900 1550
Text Label 1900 4650 3    60   ~ 0
VCC
Wire Wire Line
	1900 4550 1900 4650
Wire Wire Line
	3100 3350 3250 3350
$EndSCHEMATC
